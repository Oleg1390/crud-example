package com.example.crud.model;

public enum Statuses {
    ACTIVE, BANNED;
}
